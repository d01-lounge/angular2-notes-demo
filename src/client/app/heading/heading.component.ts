import { Component, OnInit } from '@angular/core';
import { HeadingService } from '../heading/heading.service';

/**
* This class represents the lazy loaded HomeComponent.
*/
@Component({
    moduleId: module.id,
    selector: 'heading',
    templateUrl: 'heading.component.html',
    styleUrls: ['heading.component.css'],
    directives: []
})
export class HeadingComponent implements OnInit {
    constructor(public headingService: HeadingService) {}

    ngOnInit() {
        this.headingService.title = 'Notes Angular2 Demo';
    }
}
