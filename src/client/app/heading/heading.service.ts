import { Injectable } from '@angular/core';
// import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/map';

/**
 * This class provides the NameList service with methods to read names and add names.
 */
@Injectable()
export class HeadingService {

    /**
     * The array of initial names provided by the service.
     * @type {Array}
     */
    title: string = '';

    // constructor() {}

    /**
     * Returns an Observable for the HTTP GET request for the JSON resource. If there was a previous successful request
     * (the local notes array is defined and has elements), the cached version is returned
     * @return {Notes[]} The Observable for the HTTP request.
     */
    getTitle(): Observable<string> {
        return Observable.from([this.title]);
    }

    /**
     * Adds the given name to the array of names.
     * @param {string} value - The name to add.
     */
    setTitle(title: string): void {
        this.title = title;
    }
}

