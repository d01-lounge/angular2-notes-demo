import { Component, OnInit } from '@angular/core';
import { Router, ROUTER_DIRECTIVES, Routes } from '@angular/router';
import { HTTP_PROVIDERS} from '@angular/http';

import { HeadingService } from './heading/heading.service';
import { HeadingComponent } from './heading/heading.component';
import { NotesComponent } from './notes/notes.component';
import { NotesService } from './notes/notes.service';
import { ObjectId } from './objectid/objectid';

/**
 * This class represents the main application component. Within the @Routes annotation is the configuration of the
 * applications routes, configuring the paths for the lazy loaded components (HomeComponent, AboutComponent).
 */
@Component({
    moduleId: module.id,
    selector: 'notes-app',
    viewProviders: [
        HTTP_PROVIDERS,
        NotesService,
        ObjectId,
        HeadingService
    ],
    templateUrl: 'app.component.html',
    directives: [
        ROUTER_DIRECTIVES,
        HeadingComponent
    ]
})
@Routes([
    {
        path: '/notes',
        component: NotesComponent
    }
])
export class AppComponent implements OnInit {
    constructor(private router: Router,
                            public headingService: HeadingService) {}

    ngOnInit() {
        this.router.navigate(['/notes']);
    }
}
