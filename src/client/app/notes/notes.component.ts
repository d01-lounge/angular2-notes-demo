import { FORM_DIRECTIVES } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NotesService } from './notes.service';
import { Note } from './note';
import { ObjectId } from '../objectid/objectid';
import { HeadingService } from '../heading/heading.service';

/**
 * This class represents the lazy loaded HomeComponent.
 */
@Component({
    moduleId: module.id,
    selector: 'notes',
    templateUrl: 'notes.component.html',
    styleUrls: ['notes.component.css'],
    directives: [FORM_DIRECTIVES]
})
export class NotesComponent implements OnInit {

    addingNewNote = false;
    newNote = new Note();
    newTags = '';

    /**
     * Creates an instance of the HomeComponent with the injected
     * NameListService.
     *
     * @param {NameListService} nameListService - The injected NameListService.
     */
    constructor(public notesService: NotesService,
                            public objectId: ObjectId,
                            public headingService: HeadingService) {}

    ngOnInit() {
        this.headingService.setTitle('My Notes');
    }

    updateTags() {
        this.newNote.tags = this.newTags.split(', ');
    }

    updateNote() {
        this.updateTags();
        this.newNote.id = this.objectId.timestamp();
        let now = new Date();
        this.newNote.createdAt = now.toUTCString();
        this.newNote.user = 'Bob';
    }

    clearInput() {
        this.newNote = new Note();
        this.newTags = '';
    }

    toggleNewNote(override: boolean) {
        this.addingNewNote = override;
    }

    /**
     * Calls the add method of the NameListService with the current newName value of the form.
     * @return {boolean} false to prevent default form submit behavior to refresh the page.
     */
    addNote(e: Event) {
        e.stopPropagation();
        e.preventDefault();

        this.updateNote();
        this.notesService.add(this.newNote);

        this.clearInput();
        this.toggleNewNote(false);
        return false;
    }

    cancelNewNote(e: Event) {
        e.stopPropagation();
        e.preventDefault();

        this.clearInput();
        this.toggleNewNote(false);
    }

}
