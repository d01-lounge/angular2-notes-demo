import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Note } from './note';
import 'rxjs/add/observable/from';
import 'rxjs/add/operator/map';

/**
 * This class provides the NameList service with methods to read names and add names.
 */
@Injectable()
export class NotesService {

    notes: Note[] = [];

    /**
     * The array of initial names provided by the service.
     * @type {Array}
     */

    /**
     * Contains the currently pending request.
     * @type {Observable<Notes[]>}
     */
    private request: Observable<Note[]>;

    /**
     * Creates a new NotesService with the injected Http.
     * @param {Http} http - The injected Http.
     * @constructor
     */
    constructor(private http: Http) {
    }

    /**
     * Returns an Observable for the HTTP GET request for the JSON resource. If there was a previous successful request
     * (the local notes array is defined and has elements), the cached version is returned
     * @return {Notes[]} The Observable for the HTTP request.
     */
    get(): Observable<Note[]> {
        if (this.notes && this.notes.length) {
            return Observable.from([this.notes.sort((newValue, oldValue) => {
                let newDate = new Date(newValue.createdAt).valueOf(),
                    oldDate = new Date(oldValue.createdAt).valueOf();

                return newDate > oldDate ? -1 : 1;
            })]);
        }
        if (!this.request) {
            this.request = this.http.get('/assets/data.json')
                .map((response: Response) => response.json())
                .map((data: Note[]) => {
                    this.request = null;
                    return this.notes = data;
                });
        }
        return this.request;
    }

    /**
     * Adds the given name to the array of names.
     * @param {string} value - The name to add.
     */
    add(value: Note): void {
        this.notes.push(value);
    }
}

