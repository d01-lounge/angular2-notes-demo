export class Note {
    id: string;
    tags: string[];
    createdAt: string;
    body: string;
    user: string;
};
